#!/bin/bash

os=$(grep  ^NAME /etc/*release)

if [[ $os = '/etc/os-release:NAME="Amazon Linux"' ]] ;then
	sudo service mongod stop
        sudo yum erase mongodb-org*
        sudo rm -r /var/log/mongodb
        sudo rm -r /var/lib/mongodb
elif [ $os = '/etc/os-release:NAME="Ubuntu"' ] ;then
	sudo service mongod stop
        sudo apt-get purge mongodb-org*
        sudo rm -r /var/log/mongodb
        sudo rm -r /var/lib/mongodb
fi

